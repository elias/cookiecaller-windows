import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class CookieCaller {
    public static void main(String[] args){

        String name;

        Konfiguration konfiguration = new Konfiguration();
        if(args.length == 0){
            name = konfiguration.getName();
        }else{
            name = args[0];
        }

        System.out.println("Dein Name ist " + name);

        try {
            Empfaenger empfaenger = new Empfaenger(konfiguration.getListeningIP(), konfiguration.getListeningPort(), name);
            Benachrichtigung benachrichtigung = new Benachrichtigung(konfiguration.getAppName() + " wartet auf Nachrichten... ");

            benachrichtigung.showTray();

            System.out.println("Warte auf Nachrichten auf " + konfiguration.getListeningIP().getHostAddress() + ":" + konfiguration.getListeningPort() + "... ");

            while (true) {
                empfaenger.empfangen();
                System.out.println(empfaenger.getSenderName() + ": '" + empfaenger.getSenderNachricht() + "'");
                benachrichtigung.sendNotification(empfaenger.getSenderName(), empfaenger.getSenderNachricht());
            }

        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
