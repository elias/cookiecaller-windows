import java.io.FileInputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

public class Konfiguration {
    Properties properties;

    public Konfiguration(){
        properties = new Properties();
        try {
            properties.load(new FileInputStream("resources/config.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName(){

        return properties.getProperty("name");
    }

    public String getAppName(){
        return properties.getProperty("app-name");
    }

    public Inet4Address getListeningIP() throws UnknownHostException {
        return (Inet4Address) InetAddress.getByName(properties.getProperty("listening-ip"));
    }

    public int getListeningPort(){
        return Integer.parseInt(properties.getProperty("listening-port"));
    }
}
