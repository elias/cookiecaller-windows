import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Empfaenger{
    DatagramSocket empfaenger;

    DatagramPacket anfragePacket;
    byte[] anfrage;

    DatagramPacket antwortPacket;
    byte[] antwort;

    SocketAddress address;
    public Empfaenger(Inet4Address addr, int port, String name){
        try {
            address = new InetSocketAddress(addr, port);

            empfaenger = new DatagramSocket(address);
            empfaenger.setBroadcast(true);

            anfrage = new byte[160];
            anfragePacket = new DatagramPacket(anfrage, anfrage.length, address);

            antwort = bauePacket(name);
            antwortPacket = null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void empfangen(){
        try {
            empfaenger.receive(anfragePacket);
            System.out.println("Die Nachricht von " + getName(anfrage) + " ist: " + getNachricht(anfrage));

            antwortPacket = new DatagramPacket(antwort, antwort.length, anfragePacket.getAddress(), empfaenger.getLocalPort());
            empfaenger.send(antwortPacket);
            System.out.println("Es wurde Erfolgreich mit dem Namen '" + getName(antwort) + "' geantwortet");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private byte[] bauePacket(String name){
        byte[] packet = new byte[16];

        packet[0] = 1;

        byte[] namenByte = name.getBytes(StandardCharsets.UTF_8);
        namenByte = Arrays.copyOf(namenByte, 15);


        for(int i = 1; i<= 15; i++){
            packet[i] = namenByte[i-1];
        }

        return packet;
    }

    private String getName(byte[] _anfrage){
        byte[] namenByte = new byte[15];

        for(int i = 1; i <= 15; i++){

            namenByte[i-1] = _anfrage[i];

        }

        String name = new String(namenByte, StandardCharsets.UTF_8);
        name = name.strip();

        return name;
    }

    public String getSenderName(){
        return getName(anfrage);
    }

    private String getNachricht(byte[] _anfrage){
        byte[] nachrichtByte = new byte[140];

        for(int i = 16; i<= 155; i++){
            nachrichtByte[i-16] = _anfrage[i];
        }

        String nachricht = new String(nachrichtByte, StandardCharsets.UTF_8);
        nachricht = nachricht.strip();

        return nachricht;
    }

    public String getSenderNachricht(){
        return getNachricht(anfrage);
    }

}