import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Benachrichtigung {
    SystemTray tray;
    Image icon;
    TrayIcon trayIcon;

    PopupMenu popupMenu;

    MenuItem exitButton;

    public Benachrichtigung(String tooltip){
        tray = SystemTray.getSystemTray();
        icon = Toolkit.getDefaultToolkit().getImage("resources/icon.png");

        popupMenu = new PopupMenu();
        exitButton = new MenuItem("Beenden");
        popupMenu.add(exitButton);

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });

        trayIcon = new TrayIcon(icon, tooltip, popupMenu);
        trayIcon.setImageAutoSize(true);


    }

    public void showTray(){
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendNotification(String name, String nachricht){
        trayIcon.displayMessage(name, nachricht, TrayIcon.MessageType.NONE);
    }
}
